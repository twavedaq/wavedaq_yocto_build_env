# Linux for PSI WaveDAQ Data Concentrator Board (DCB) - Yocto Build Environment

This GIT repository contains the files needed to configure the yocto environment to build u-boot, kernel and root file system
for the DCB of the MEGII project.

This repository only contains a few configuration files and is considered to be submodule of a repository that contains all
necessary sources for the build including the open source layers from yocto and openembedded as well as the custom PSI layer.



## Configuration Files

### local.conf

This file contains all settings to configure the build environment variables.

### bblayers.conf

This file contains the locations of all layers to be included into the yocto build.



## Building U-Boot, Kernel and Rootfilesystem

To build the Linux targets, this repository has to be checked out as submodule of the wavedaq_linux repo.
If this is the case, a description of the build procedure can be found in the parent repositorys [README.md](../../README.md)
file (two directory levels above).

## Submodules

The repository submodules reside in the following paths:

| Repository                  | Path                      | Comment                                         |
|-----------------------------|---------------------------|-------------------------------------------------|
| wavedaq_linux               | (root of linux repos)     | Linux main repo containing the other submodules |
| **wavedaq_yocto_build_env** | **system/dcb_rootfs_lsb** | **Custom Main Yocto Configuration**             |
| wavedaq_yocto_layer         | layer/meta-psi-wavedaq    | Custom Layer                                    |
| Poky                        | layer/poky                | Yocto (Reference Distribution)                  |
| OpenEmbedded                | layer/meta-openembedded   | Yocto (Build System)                            |
